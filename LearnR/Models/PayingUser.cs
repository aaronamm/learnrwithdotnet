﻿using System;

namespace LearnR.Models
{
    public class PayingUser
    {
        public int UserId { get; set; }
        public string CreditAction { get; set; }
        public int Amount { get; set; }
        public int CurrentCandy { get; set; }
        public DateTime CreateDate { get; set; }//from timezoneOffset 7

    }
}