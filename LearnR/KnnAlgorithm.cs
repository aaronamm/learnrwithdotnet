﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using RDotNet;

namespace LearnR
{

    public class KnnAlogorithm
    {

        public void Run()
        {
            using (REngine engine = REngine.CreateInstance("RDotNet"))
            {
                engine.Initialize(); // required since v1.5

                //Now create a string expression to form a DataFrame:
                // wbcd <- read.csv("wisc_bc_data.csv", stringsAsFactors = FALSE)
                string dataFrameExpression = @"wbcd <- read.csv( 
                                                        'Data/wisc_bc_data.csv',
                                                        stringsAsFactors=FALSE)";

                var wbcdDateFrame = engine.Evaluate(dataFrameExpression).AsDataFrame();

                engine.Evaluate("str(wbcd)");
                Console.Write("\n\n");

                //remove id column
                engine.Evaluate("wbcd <- wbcd[-1]");

                engine.Evaluate("print(table(wbcd$diagnosis))");

                //set Label like change to meaningfull word
                engine.Evaluate(@"wbcd$diagnosis <- factor(
                                  wbcd$diagnosis, levels = c('B', 'M'),
                                  labels = c('Begin', 'Malignant')
                                  )");

                engine.Evaluate("str(wbcd$diagnosis)");


                //create function
                engine.Evaluate(@"normalize <- function(x) { 
                                    return ((x - min(x)) / (max(x) - min(x))) 
                                    }");

                engine.Evaluate(@"wbcd_n <- as.data.frame(lapply(wbcd[2:31], normalize))");

                engine.Evaluate(@"print(summary(wbcd_n$area_mean))");

                engine.Evaluate(@"wbcd_train <- wbcd_n[1:469, ]");
                engine.Evaluate(@"wbcd_test <- wbcd_n[470:569, ]");

                engine.Evaluate("wbcd_train_labels <- wbcd[1:469, 1]");
                engine.Evaluate("wbcd_test_labels <- wbcd[470:569, 1]");

                engine.Evaluate("library(class)");
                engine.Evaluate(@"wbcd_test_pred <- knn( 
                                      train = wbcd_train, 
                                      test = wbcd_test,
                                      cl = wbcd_train_labels, 
                                      k=21)");
                engine.Evaluate("library(gmodels)");

                engine.Evaluate(@"CrossTable(x = wbcd_test_labels, y = wbcd_test_pred, prop.chisq=FALSE)");
            }
        }


    }//end class
}//end namespace
