﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using RDotNet;
using log4net.Config;

namespace LearnR
{


                //load from CSV
                //http://www.codeproject.com/Articles/25133/LINQ-to-CSV-library
                //https://github.com/mperdeck/LINQtoCSV
                //http://stackoverflow.com/questions/14966178/using-linq-for-csv-data
                //http://stackoverflow.com/questions/3507498/reading-csv-file
                //http://filehelpers.sourceforge.net/
                //http://www.codeproject.com/Articles/11698/A-Portable-and-Efficient-Generic-Parser-for-Flat-F
    class Program
    {

//simple commment
        static void Main(string[] args)
        {

            XmlConfigurator.Configure();

            SetupPath();//load R to current process

            //GettingStart.Start();
            //var knnAlogorithm = new KnnAlogorithm();
            //knnAlogorithm.Run();

            var dataFrameFromSqlServer = new DataFrameFromSqlServer();
            dataFrameFromSqlServer.Read();
        }

        public static void SetupPath()
        {
            var oldPath = Environment.GetEnvironmentVariable("PATH");
            var rPath = Environment.Is64BitProcess ? @"C:\Program Files\R\R-3.1.1\bin\x64"
                : @"C:\Program Files\R\R-3.1.1\bin\i386";
            // Mac OS X
            //var rPath = "/Library/Frameworks/R.framework/Libraries";
            // Linux (in case of libR.so exists in the directory)
            //var rPath = "/usr/lib";
            if (Directory.Exists(rPath) == false)
                throw new DirectoryNotFoundException(
                    string.Format("Could not found the specified path to the directory containing R.dll: {0}", rPath));
            var newPath = string.Format("{0}{1}{2}", rPath, System.IO.Path.PathSeparator, oldPath);
            System.Environment.SetEnvironmentVariable("PATH", newPath);
            // NOTE: you may need to set up R_HOME manually also on some machines
            string rHome = "";
            var platform = Environment.OSVersion.Platform;
            switch (platform)
            {
                case PlatformID.Win32NT:
                    break; // R on Windows seems to have a way to deduce its R_HOME if its R.dll is in the PATH
                case PlatformID.MacOSX:
                    rHome = "/Library/Frameworks/R.framework/Resources";
                    break;
                case PlatformID.Unix:
                    rHome = "/usr/lib/R";
                    break;
                default:
                    throw new NotSupportedException(platform.ToString());
            }
            if (!string.IsNullOrEmpty(rHome))
                Environment.SetEnvironmentVariable("R_HOME", rHome);
        }//end method
    }

}//end namespace
