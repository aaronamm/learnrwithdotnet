﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Data.Sql;
using LINQtoCSV;
using LearnR.Models;
using RDotNet;
using log4net;

namespace LearnR
{
    public class DataFrameFromSqlServer
    {
        private ILog _log = LogManager.GetLogger(typeof(DataFrameFromSqlServer).Name);

        public void Read()
        {
            var connectionString = ConfigurationManager.AppSettings["connectionString"];
            _log.DebugFormat("connectionString: {0}", connectionString);

            var payingUsers = new List<PayingUser>();
            using (var connection = new SqlConnection(connectionString))
            using (var command = connection.CreateCommand())
            {

                var sql = @"SELECT TOP 10 User_id AS UserId, CreditAction, Amount,  0 AS CurrentCandy, 
                          CONVERT(datetime, CreateDate, 0) AS CreateDate FROM CreditTransaction";

                command.CommandText = sql;
                connection.Open();

                var reader = command.ExecuteReader();
                while (reader.Read())
                {
                    //for (int i = 0; i < reader.FieldCount; i++)
                    //{
                    //    var name = reader.GetName(i);
                    //    var value = reader.GetValue(i);
                    //    _log.DebugFormat("name: {0}, value: {1}", name, value);
                    //}
                    payingUsers.Add(new PayingUser()
                    {
                        Amount = Convert.ToInt32(reader["Amount"]),
                        CreateDate = Convert.ToDateTime(reader["CreateDate"]),
                        CreditAction = Convert.ToString(reader["CreditAction"]),
                        CurrentCandy = Convert.ToInt32(reader["CurrentCandy"]),
                        UserId = Convert.ToInt32(reader["UserId"])
                    });

                }
            }//end connection


            //conver to CSV
            var outputFileDescription = new CsvFileDescription
            {
                SeparatorChar = ',',
                FirstLineHasColumnNames = true,
            };
            var cc = new CsvContext();
            string csvString;
            using (var memoryStream = new MemoryStream())
            using (var streamWrtier = new StreamWriter(memoryStream, Encoding.UTF8))
            {
                cc.Write(payingUsers, streamWrtier, outputFileDescription);
                streamWrtier.Flush();
                // memoryStream.Position = 0;
                csvString = Encoding.UTF8.GetString(memoryStream.ToArray());
            }

            _log.DebugFormat("csvString: \n\n{0}", csvString);

            using (var engine = REngine.CreateInstance("RDotNet"))
            {
                engine.Initialize(); // required since v1.5


                engine.Evaluate( string.Format(@"csvString <- ""{0}""",csvString) );
                engine.Evaluate(@"data <- read.csv(con <- textConnection(csvString), header=TRUE)");
                engine.Evaluate(@"close(con)");


                //output


                engine.Evaluate("print(data)");
                engine.Evaluate("print(data$UserId)");



            }

            Console.Read();
        }//end method

    }
}
