﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using RDotNet;

namespace LearnR
{

    public class User
    {
        public int Id { get; set; }
        public int Age { get; set; }
        public string Gender { get; set; }
    }

    public class GettingStart
    {

        public static void Start()
        {

            //REngine.SetEnvironmentVariables(); // Currently under development - coming soon
    
            using (REngine engine = REngine.CreateInstance("RDotNet"))
            {
                engine.Initialize(); // required since v1.5

                //load from CSV
                //http://www.codeproject.com/Articles/25133/LINQ-to-CSV-library
                //https://github.com/mperdeck/LINQtoCSV

                //So for example you may have two columns in your DataTable and this would give you something like:
                var users = new List<User>() { 
                    new User(){ Id = 1, Age = 25, Gender = "M"},
                    new User(){ Id = 2, Age = 28, Gender = "F"},
                    new User(){ Id = 3, Age = 21, Gender = "F"},
                    new User(){ Id = 4, Age = 30, Gender = "F"},
                    new User(){ Id = 5, Age = 29, Gender = "M"},
                    new User(){ Id = 6, Age = 35, Gender = "M"},
                    new User(){ Id = 7, Age = 22, Gender = "F"},
                    new User(){ Id = 8, Age = 23, Gender = "M"},
                    new User(){ Id = 9, Age = 27, Gender = "F"},
                    new User(){ Id = 10, Age = 31, Gender = "F"}
                };

                List<int> idColumn = users.Select(u => u.Id).ToList();
                List<int> ageColumn = users.Select(u => u.Age).ToList();
                List<string> genderColumn = users.Select(u => u.Gender).ToList();


                //Now create a NumericVector for each one eg:
                engine.SetSymbol("id", engine.CreateIntegerVector(idColumn));
                engine.SetSymbol("age", engine.CreateIntegerVector(ageColumn));
                engine.SetSymbol("gender", engine.CreateCharacterVector(genderColumn));


                //Now create a string expression to form a DataFrame:
                string dataFrameExpression = @"users <- data.frame( 
                                                        id = id,
                                                        age= age, 
                                                        gender= gender,
                                                        stringsAsFactors=FALSE)";

                //Now do something like:
                var userDateFrame = engine.Evaluate(dataFrameExpression).AsDataFrame();
                engine.Evaluate("str(users)");
                engine.Evaluate("print(users)");
                Console.Write("\n\n");

                engine.Evaluate("print(users$gender)");
                Console.Write("\n\n");


                //create factor new variable userGender as Factor
                engine.Evaluate(@"usersGender <- factor(users$gender, levels = c('M', 'F'), labels = c('Male', 'Female'))");
                Console.Write("\n\n");

                //output structure of usersGender 
                engine.Evaluate("str(usersGender)");
                Console.Write("\n\n");

                //show number of user group by gender
                engine.Evaluate("print(table(usersGender))");
                Console.Write("\n\n");

                //in percentage
                engine.Evaluate("print( round(prop.table(table(usersGender)) * 100, digits = 1) )");
                Console.Write("\n\n");

                //summary user data frame only age column
                engine.Evaluate("str(users$age)");
                engine.Evaluate("print(summary(users$age))");



                Console.ReadKey();
            }
        }

    }
}//end namespace
